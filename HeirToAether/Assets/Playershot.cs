﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playershot : MonoBehaviour {
    // Use this for initialization
    void Start () {
        gameObject.transform.parent = gameObject.transform;
        Destroy(this.gameObject, 4f);
        GetComponent<Rigidbody>().velocity = transform.forward * -17;
        GetComponent<AudioSource>().Play();
}
	
	// Update is called once per frame
	void Update () {
		
	}
}
